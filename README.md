# Vocabulary Trainer

**Team**  
This project was made by: Jasper J. J. Westra

**Intro**
A project that helps you train vocabulary of a new language or help you remember professional jargon. By entering the words from the two languages you start learning the moment you begin making a new list. When your list is done you can practice in a multiple-choice style quiz. The system will make a sound that tells you you have answered wrong or correct. This auditory feedback helps to remember the words.

When you feel like you are answering everything correct it is possible to switch languages. This way you can learn the words in both directions.

**Why this project**
When I was young I was struggling to remember vocabulary from French, German & English which are mandatory to learn in the Netherlands. Just writing down the words over and over doesn't work for me (and a lot of others). I realised that in order to remember the lists more efficiently the order of writing the words down should be random. On the school pcs's I found exam software for making school exams, with a multiple-choice option. I would make my own exams and study from that. The software had an option to shuffle the answers.
 
I read about the hackathon in the Supabase Discord and this idea came to mind. I really like Supabase, and I have been using it for over a year now. I think it is Great!

## Live application URL

Please visit: [vocabulary.center](https://vocabulary.center)

Here you will find the project in a live environment. I plan to leave it running here forever; whilst improving the project over time.

![Homepage](https://vocabulary.center/screens/1.png)
![List editor](https://vocabulary.center/screens/4.png)
![Mobile quiz](https://vocabulary.center/screens/2.png)
![Mobile, my lists](https://vocabulary.center/screens/3.png)

## Development server

  

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Features

* The application uses Supabase's authentication to manage accounts.
	* With an account you can manage private and public word lists
* Data is stored with Supabase
	* On the homepage are the last 30 publicly submitted wordlists
	* When logged in you can see your own private and public lists
* Snowflakes, when you want you can turn on a snow effect
* The loading spinner is a snowflake 

## Plans for the future
* Eventually there will be an app for android, ios and windows devices
* A way to mark lists as a favourite, and a page where you can keep track of your favourites
* A public profile
	* On this profile you can make sections with wordlists. You can then share this list with other people. This is especially good for high schools where kids have to learn word lists.
* Real-time
	* On the homepage, there should be real-time statistics
	* If a list is public you can see in realtime how many other users are doing this list
* Use text to speech to read out the words