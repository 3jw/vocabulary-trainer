import { Component, OnInit } from '@angular/core';
import { QuizService } from './quiz.service';
import { SupabaseService } from './supabase.service';
import { YesnoService } from './yesno.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  session = this.supa.session;
  name=""
  menuopen = false

  snowOn=false

  constructor(private supa: SupabaseService, public yesno:YesnoService, public quiz: QuizService) {

  }

  ngOnInit() {
    this.supa.authChanges((_, session) => {
      this.session = session
      this.name = this.supa?.user?.email
    });
  }

  logout() {
    this.supa.signOut()
  }

  closeMenu(e) {
    if (e.target.id != 'hamburger') {
      this.menuopen = false
    }
  }
  
}
