import { Injectable } from '@angular/core';
import { AuthChangeEvent, createClient, Session, SupabaseClient, UserCredentials } from '@supabase/supabase-js'
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SupabaseService {

  public supabase: SupabaseClient;

  constructor() {
    this.supabase = createClient(environment.supabaseUrl, environment.supabaseKey)
  }

  public get user() {
    return this.supabase.auth.user();
  }

  public get session() {
    return this.supabase.auth.session();
  }

  public authChanges(callback: (event: AuthChangeEvent, session: Session | null) => void) {
    return this.supabase.auth.onAuthStateChange(callback);
  }

  signIn(credentials: UserCredentials) {
    return this.supabase.auth.signIn(credentials);
  }

  public signOut() {
    return this.supabase.auth.signOut();
  }

  async signInWithFacebook() {
    const { user, session, error } = await this.supabase.auth.signIn({
      provider: 'facebook'
    })
  }

  async signInWithDiscord() {
    const { user, session, error } = await this.supabase.auth.signIn({
      provider: 'discord',
    })
  }
  
  async signInWithGoogle() {
    const { user, session, error } = await this.supabase.auth.signIn({
      provider: 'google',
    })
  }
}
