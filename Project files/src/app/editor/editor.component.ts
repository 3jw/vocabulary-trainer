import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SupabaseService } from '../supabase.service';
import { YesnoService } from '../yesno.service';
import { ToastrService } from 'ngx-toastr';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {


  ready = false
  id = undefined
  rows: string[][] = undefined
  title = ""
  name1 = ""
  name2 = ""
  ispublic = false

  isSaved = true;

  subscription: Subscription = null

  constructor(private supa: SupabaseService, private route: ActivatedRoute, private router: Router, private yesno: YesnoService, private toastr: ToastrService, private quiz: QuizService) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.init()
  }

  async init() {
    this.isSaved = true;
    if (this.id) {
      let {data, error} = await this.supa.supabase.from('list').select('name1,name2,title,data,ispublic').eq('id', this.id).single()
      if (error) {
        console.log(error);
        this.router.navigate(['/mylists'])
      } else {
        this.title = data.title;
        this.name1 = data.name1;
        this.name2 = data.name2;
        this.ispublic = data.ispublic;
        this.rows = data.data;
        this.ready = true
      }
    } else {
      this.rows = []
      this.ready = true
    }
  }

  addRow(index) {
    this.rows.splice((index<0?this.rows.length:index), 0, ["",""]);
    this.isSaved = false;
  }
  
  delRow(index) {
    this.rows.splice(index, 1)
    this.isSaved = false;
  }

  canSave():boolean {
    if (this.title.length < 3) {
      this.toastr.warning('Please provide a title for your list');
      return false;
    }
    if (this.name1.length < 3) {
      this.toastr.warning('Please enter the first language name',);
      return false;
    }
    if (this.name2.length < 3) {
      this.toastr.warning('Please enter the second language name');
      return false;
    }
    if (this.rows.length < 4) {
      this.toastr.warning('You need to have at least 4 words with a translation');
      return false
    }
    if (this.rows.findIndex(p => p[0] == "" || p[1] == "") >= 0) {
      this.toastr.warning('Some words are not translated');
      return false
    }
    return true
  }

  async back() {
    if (!this.isSaved) {
      this.yesno.openDialog({
        title: 'Do you want to leave without saving?',
        yestext: 'Yes',
        notext: 'Cancel'
      })
      
      this.subscription = this.yesno.answerSubject.subscribe(v => {
        this.subscription.unsubscribe()
        if (v == 'Yes') {
          this.router.navigate(['/mylists'])
        }
      })
    } else {
      this.router.navigate(['/mylists'])
    }
  }

  async delete() {
    if (this.id) {
      this.yesno.openDialog(YesnoService.DeleteData)
      this.subscription = this.yesno.answerSubject.subscribe(async v => {
        this.subscription.unsubscribe()
        if (v == 'Delete') {
          this.toastr.success("Deleted the list!");
          let {data,error} = await this.supa.supabase.from('list').delete({returning: 'minimal'}).eq('id', this.id)
          this.router.navigate(['/mylists'])
        }
      })
    }
  }

  async save() {
    if (!this.canSave()) {
      return
    }
    this.ready = false
    if (this.id) { 
      let {data,error} = await this.supa.supabase.from('list').update({
        title: this.title,
        name1: this.name1,
        name2: this.name2,
        ispublic: this.ispublic,
        data: this.rows
      }).eq('id', this.id).single()
      this.toastr.success("Saved!");
      this.isSaved = true;
    } else {
      let {data,error} = await this.supa.supabase.from('list').insert({
        user_id: this.supa.user.id,
        title: this.title,
        name1: this.name1,
        name2: this.name2,
        ispublic: this.ispublic,
        data: this.rows
      }).single()
      this.id = data.id
      this.toastr.success("Saved!");
      this.router.navigate(['/mylists/editor', {id: data.id}])
    }
    this.ready = true
  }

  play() {
    if (!this.canSave()) {
      return;
    }
    this.quiz.name1 = this.name1;
    this.quiz.name2 = this.name2;
    this.quiz.quizdata = this.rows;
    this.quiz.openSubject.next(true)
  }

}
