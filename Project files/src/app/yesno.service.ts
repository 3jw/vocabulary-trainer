import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class YesnoService {

  public openSubject: BehaviorSubject<YesNoData> = new BehaviorSubject(undefined);
  public answerSubject: Subject<string> = new Subject();

  constructor() {
    this.answerSubject.subscribe(v => {
      this.openSubject.next(undefined)
    })
  }

  openDialog(data: YesNoData) {
    this.openSubject.next(data)
  }

  closeDialog() {
    this.answerSubject.next(undefined);
  }

  public static SaveData: YesNoData = {
    title: 'Do you want to save?',
    yestext: 'Yes',
    notext: 'Cancel'
  }
  
  public static DeleteData: YesNoData = {
    title: 'Do you want to delete this item?',
    yestext: 'Delete',
    notext: 'Cancel'
  }
}

export interface YesNoData {
  title: string,
  yestext: string,
  notext: string
}
