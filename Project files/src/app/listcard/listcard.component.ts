import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-listcard',
  templateUrl: './listcard.component.html',
  styleUrls: ['./listcard.component.scss']
})
export class ListcardComponent implements OnInit {

  @Input('title') title = "Title"
  @Input('public') public = true
  @Input('languages') languages = "Languages"
  @Input('created') created = ""
  
  constructor() { }

  ngOnInit(): void {
  }

}
