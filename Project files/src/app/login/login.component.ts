import { Component, OnInit } from '@angular/core';
import { SupabaseService } from '../supabase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private supa: SupabaseService) { }

  fb() {
    this.supa.signInWithFacebook()
  }

  dc() {
    this.supa.signInWithDiscord()
  }
  
  gg() {
    this.supa.signInWithGoogle()
  }

  ngOnInit(): void {
  }

}
