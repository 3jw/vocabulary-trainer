import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  public name1: string;
  public name2: string;
  public quizdata: string[][];
  public openSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }
}
