import { Component, OnInit } from '@angular/core';

import { Howl, Howler } from 'howler';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  constructor(private quiz: QuizService, private toastr: ToastrService) { }

  correct = new Howl({
    src: [environment.correctsound],
    html5: true
  });

  wrong = new Howl({
    src: [environment.wrongsound],
    html5: true
  });

  name1 = "NL"
  name2 = "EN"

  qtext = '';
  qans = 1
  ans1 = ""
  ans2 = ""
  ans3 = ""
  ans4 = ""

  normal = true;

  // questions = [['hier', 'hoena'], ['daar', 'hoenaaka'], ['boven', 'faoeka'], ['onder', 'tahhte'], ['voor', 'amaama'], ['galfa', 'achter'], ['tussen', 'beina'], ['rondom', 'haoela'], ['in het midden', 'wasadtoh'], ['naast', 'zjeniba'], ['ochtend', 'ss`obahan'], ['avond', "masaa' an"], ['morgan', '3radan'], ['tijdens', "ithna'a"], ['terwijl/when', 'hhiena'], ['nacht', 'laylan'], ['dag', 'naharan'], ['bloemen', 'zoqer'], ['tafel', 'tauwila'], ['muis (pc)', 'fahra'], ['thee', 'shay'], ['mint', 'mahna'], ['water', "ma'eh"], ['pistache', 'foestoq'], ['bakje', 'zubdiee'], ['bord', 'zacher'], ['takje', 'miswak'], ['inside', 'dagila'], ['hart', 'qalb'], ['dichtbij', 'haatha, haathihie'], ['deze / die / mv', "haa oelaa'ie, oelaa'ika"], ['dat', "thalieka, oelaa'ika"]]
  questions = [
    // singular
    ['I', 'أنا'],
    ['you (masc.)', 'انتَ'],
    ['you (fem.)', 'انتِ'],
    ['he', 'هو'],
    ['she', 'هي'],
    // dual
    ['we', 'نحن'],
    ['you (dual)', 'أنتما'],
    ['they (dual)', 'هما'],
    // plural
    ['you (plural masc.)', 'أنتم'],
    ['you (plural fem.)', 'أنتن'],
    ['they (plural masc.)', 'هم'],
    ['they (plural fem.)', 'هن'],

    //possessive singular
    ['my', 'ـي'],
    ['your (masc.)', 'ـكَ'],
    ['your (fem.)', 'ـكِ'],
    ['his', 'ـه'],
    ['her', 'ـها'],
    // dual
    ['our', 'ـنا'],
    ['your (dual)', 'ـكما'],
    ['their (dual)', 'ـهما'],
    // plural
    ['your (plural masc.)', 'ـكم'],
    ['your (plural fem.)', 'ـكن'],
    ['their (plural masc.)', 'ـهم'],
    ['their (plural fem.)', 'ـهن'],
  ]

  answered(n) {
    if (n == this.qans) {
      this.correct.play()
      this.nextQ()
    } else {
      this.toastr.info('Try again!', undefined, {timeOut: 2000, positionClass: 'toast-top-center'})
      this.wrong.play()
    }
  }

  nextQ() {
    let _answ = []
    for (let i = 0; i < 4; i++) {
      let rq = -1
      while (rq < 0 || _answ.indexOf(this.questions[rq]) >= 0) {
        rq = Math.round(Math.random() * (this.questions.length - 1))
      }
      _answ.push(this.questions[rq]);
    }
    let newAns = Math.round(Math.random() * 3) + 1;
    let newText = this.normal ? _answ[newAns - 1][0] : _answ[newAns - 1][1]
    while (newText == this.qtext) {
      newAns = Math.round(Math.random() * 3) + 1;
      newText = this.normal ? _answ[newAns - 1][0] : _answ[newAns - 1][1]
    }
    this.qans = newAns
    this.qtext = newText
    this.ans1 = this.normal ? _answ[0][1] : _answ[0][0]
    this.ans2 = this.normal ? _answ[1][1] : _answ[1][0]
    this.ans3 = this.normal ? _answ[2][1] : _answ[2][0]
    this.ans4 = this.normal ? _answ[3][1] : _answ[3][0]
    console.log(this.qans, _answ);
  }

  toggleNormal() {
    this.normal = !this.normal;
    this.nextQ();
  }

  ngOnInit() {
    this.name1 = this.quiz.name1;
    this.name2 = this.quiz.name2;
    this.questions = this.quiz.quizdata;
    this.nextQ();
    console.log(location.href);
  }

  exit() {
    this.quiz.openSubject.next(undefined);
  }

}
