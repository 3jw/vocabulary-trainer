import { Component, OnInit } from '@angular/core';
import { SupabaseService } from '../supabase.service';

@Component({
  selector: 'app-mylists',
  templateUrl: './mylists.component.html',
  styleUrls: ['./mylists.component.scss']
})
export class MylistsComponent implements OnInit {

  lists = undefined

  constructor(private supa: SupabaseService) { }

  ngOnInit(): void {
    this.init();
  }

  async init() {
    let {data,error} = await this.supa.supabase.from('list').select('title,name1,name2,id,created_at,ispublic').eq('user_id', this.supa.user.id)
    console.log(data, error);
    if (data) {
      this.lists = data
    }
  }

}
