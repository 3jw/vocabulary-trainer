import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { EditorComponent } from './editor/editor.component';
import { LoginComponent } from './login/login.component';
import { TrainerComponent } from './trainer/trainer.component';

import { RouterModule, Routes } from '@angular/router';
import { PageheaderComponent } from './pageheader/pageheader.component';
import { MylistsComponent } from './mylists/mylists.component';
import { ListcardComponent } from './listcard/listcard.component';
import { FormsModule } from '@angular/forms';
import { LoaderComponent } from './loader/loader.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { TogglebtnComponent } from './togglebtn/togglebtn.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'mylists', component: MylistsComponent },
  { path: 'mylists/editor', component: EditorComponent },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EditorComponent,
    LoginComponent,
    TrainerComponent,
    PageheaderComponent,
    MylistsComponent,
    ListcardComponent,
    LoaderComponent,
    TogglebtnComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HammerModule,
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),
    BrowserAnimationsModule,
    ToastrModule.forRoot({ progressBar: true, timeOut: 3000 })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
