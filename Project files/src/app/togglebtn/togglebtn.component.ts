import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-togglebtn',
  templateUrl: './togglebtn.component.html',
  styleUrls: ['./togglebtn.component.scss']
})
export class TogglebtnComponent {

  @Input('ischecked') ischecked = false
  @Output('ischeckedChange') ischeckedChange: EventEmitter<boolean> = new EventEmitter<boolean>()

  constructor() { }

  toggle() {
    this.ischecked = !this.ischecked
    this.ischeckedChange.emit(this.ischecked)
  }

}
