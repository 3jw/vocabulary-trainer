import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { SupabaseService } from '../supabase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  lists = undefined
  ready = false

  constructor(private supa: SupabaseService, private quiz: QuizService) { }

  ngOnInit(): void {
    this.init();
  }

  isLoggedOut() {
    return !this.supa.user
  }

  async init() {
    let {data,error} = await this.supa.supabase.from('list').select('title,name1,name2,id,created_at').eq('ispublic', true).limit(30)
    console.log(data, error);
    if (data) {
      this.lists = data
      this.ready = true
    }
  }

  async play(id) {
    this.ready = false;
    let {data,error} = await this.supa.supabase.from('list').select('title,name1,name2,data').eq('id', id).single()
    this.quiz.name1 = data.name1;
    this.quiz.name2 = data.name2;
    this.quiz.quizdata = data.data;
    this.ready = true;
    this.quiz.openSubject.next(true)
  }

}
